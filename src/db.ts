'user strict';

import mysql from 'mysql';

// local mysql db connection
const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'test1234',
    database : 'bartender'
});

connection.connect((err) => {
    if (err) throw err;
});

export default connection;