import { IngredientCategory } from './ingredient-category';
import { IngredientUnit } from './ingredient-unit';

export class Ingredient {
    id: number;
    name: string;
    category: IngredientCategory;
    image_src?: string;
    unit?: IngredientUnit;
}
