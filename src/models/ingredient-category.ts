export enum IngredientCategory {
    alcohol = 'alcohol',
    juice = 'juice',
    syrup = 'syrup',
    softdrink = 'softdrink',
    other = 'other'
}