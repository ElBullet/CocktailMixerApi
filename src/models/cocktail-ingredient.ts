import { Ingredient } from './ingredient';

export class CocktailIngredient {
    ingredient_id: number;
    cocktail_id: number;
    ingredient: Ingredient;
    amount: number;
}