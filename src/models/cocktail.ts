import { CocktailCategory } from './cocktail-category';
import { CocktailIngredient } from './cocktail-ingredient';

export class Cocktail {
    id: number;
    name: string;
    category: CocktailCategory;
    image_src?: string;
    ingredients: CocktailIngredient[];
}
