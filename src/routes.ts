import app from './app';

// Controllers (route handlers)
import * as ingredientController from './controllers/ingredients';
import * as cocktailController from './controllers/cocktails';

export const register = () => {
    // ingredients
    app.get('/api/ingredients', ingredientController.getAll);
    app.get('/api/ingredients/:id', ingredientController.get);
    app.post('/api/ingredients', ingredientController.create);

    // cocktails
    app.get('/api/cocktails', cocktailController.getAll);
    app.get('/api/cocktails/:id', cocktailController.get);
    app.get('/api/cocktails/:id/ingredients', cocktailController.getIngredients);
    app.post('/api/cocktails', cocktailController.create);
};