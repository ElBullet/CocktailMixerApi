import { Observable } from 'rxjs';
import db from '../db';
import { Cocktail } from '../models/cocktail';
import { CocktailIngredient } from '../models/cocktail-ingredient';

const tableName = 'cocktail';
export const getAll = (page = 0, pageSize = 10): Observable<Cocktail[]> => {
    return new Observable((observer) => {
        db.query(`SELECT * FROM ${tableName} LIMIT ${page * pageSize}, ${pageSize}`, (err, result) => {
            if (err) throw err;
            observer.next(result);
        });
    });
};

export const getIngredients = (cocktailId: number): Observable<CocktailIngredient[]> => {
    return new Observable((observer) => {
        db.query(`SELECT * FROM cocktail_ingredient ci
        INNER JOIN ingredient i on i.id = ci.ingredient_id
        WHERE ci.cocktail_id = ${db.escape(cocktailId)}`, (err, ingredients: CocktailIngredient[]) => {
                if (err) throw err;
                ingredients = ingredients.map((value: any) => {
                    return {
                        ingredient_id: value.ingredient_id,
                        cocktail_id: value.cocktail_id,
                        amount: value.amount,
                        ingredient: {
                            id: value.id,
                            name: value.name,
                            category: value.category,
                            unit: value.unit,
                            image_src: value.image_src,
                        }
                    };
                });
                observer.next(ingredients);
            });
    });
};

export const getByIngredients = (ingredientIds: number[]): Observable<Cocktail[]> => {
    // selects all cocktail ids that can not be done with the given ingredients and then inverts the result
    const query = `SELECT * from ${tableName}
    WHERE id NOT IN
    (SELECT DISTINCT cocktail_id FROM cocktail_ingredient
        INNER JOIN ingredient ON ingredient_id = ingredient.id
    WHERE ingredient.unit = 'cl' AND ingredient_id NOT IN (${db.escape(ingredientIds.join(','))}))`;

    return new Observable((observer) => {
        db.query(query, (err, result: Cocktail[]) => {
            if (err) throw err;
            observer.next(result);
        });
    });
};

export const add = (cocktail: Cocktail): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`INSERT INTO ${tableName} (name, category, image_src) VALUES (?,?,?)`,
            [cocktail.name, cocktail.category, cocktail.image_src],
            (err, result: any) => {
                if (err) throw err;
                cocktail.id = result.insertId;
                // build insert statement for cocktail ingredients
                let ingredientsQuery = 'INSERT INTO cocktail_ingredient (cocktail_id, ingredient_id, amount) VALUES ';
                for (const ingredient of cocktail.ingredients) {
                    ingredientsQuery += `(${cocktail.id}, ${db.escape(ingredient.ingredient_id)}, ${db.escape(ingredient.amount)}), `;
                }
                ingredientsQuery = ingredientsQuery.slice(0, -2);
                db.query(ingredientsQuery, (err2) => {
                    if (err2) throw err;
                    observer.next(true);
                });
            });
    });
};

export const find = (id: number): Observable<Cocktail> => {
    return new Observable((observer) => {
        const query = `SELECT * FROM ${tableName} c WHERE c.id = ${db.escape(id)} LIMIT 1`;
        db.query(query, (err, result: any[]) => {
            if (err) throw err;
            observer.next(result[0]);
        });
    });
};

export const update = (cocktail: Cocktail): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`UPDATE ${tableName} SET name = ?, category = ?, image_src = ? WHERE id = ?`,
            [cocktail.name, cocktail.category, cocktail.image_src, cocktail.id],
            err => {
                if (err) throw err;
                observer.next(true);
            });
    });
};

export const remove = (id: number): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`DELETE FROM ${tableName} WHERE id = ?`, [id],
            err => {
                if (err) throw err;
                observer.next(true);
            });
    });
};
