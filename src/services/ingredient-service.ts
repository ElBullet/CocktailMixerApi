import { Ingredient } from '../models/ingredient';
import { Observable } from 'rxjs';
import db from '../db';

const tableName = 'ingredient';
export const getAll = (filter: any, page = 0, pageSize = 10): Observable<Ingredient[]> => {
    let query = `SELECT * FROM ${tableName}`;
    if (filter) {
        const keys = Object.keys(filter);
        if (keys.length > 0) {
            query += ' WHERE ';
            for (let i = 0; i < keys.length; ++i) {
                const prop = keys[i];
                const value = db.escape(`%${filter[prop]}%`);
                if (i === keys.length - 1) {
                    query += `\`${prop}\` LIKE ${value}`;
                } else {
                    query += `\`${prop}\` LIKE ${value} AND `;
                }
            }
        }
    }

    return new Observable((observer) => {
        db.query(`${query} LIMIT ${page * pageSize}, ${pageSize}`, (err, ingredients: Ingredient[]) => {
            if (err) throw err;
            observer.next(ingredients);
        });
    });
};

export const add = (ingredient: Ingredient): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`INSERT INTO ${tableName} (name, category, image_src, unit) VALUES (?,?,?,?)`,
            [ingredient.name, ingredient.category, ingredient.image_src, ingredient.unit],
            (err, result: any) => {
                if (err) throw err;
                ingredient.id = result.insertId;
                observer.next(true);
            });
    });
};

export const find = (id: string): Observable<Ingredient> => {
    return new Observable((observer) => {
        db.query(`SELECT * FROM ${tableName} WHERE id = ${db.escape(id)} LIMIT 1`, (err, result: Ingredient[]) => {
            if (err) throw err;
            observer.next(result[0]);
        });
    });
};

export const exists = (id: string): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`SELECT id FROM ${tableName} WHERE id = ${db.escape(id)} LIMIT 1`, (err, ids: number[]) => {
            if (err) throw err;
            observer.next(ids.length === 1 ? true : false);
        });
    });
};

export const update = (ingredient: Ingredient): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`UPDATE ${tableName} SET name = ?, category = ?, image_src = ?, unit = ?
        WHERE id = ?`,
            [ingredient.name, ingredient.category, ingredient.image_src, ingredient.unit, ingredient.id],
            err => {
                if (err) throw err;
                observer.next(true);
            });
    });
};

export const remove = (id: number): Observable<boolean> => {
    return new Observable((observer) => {
        db.query(`DELETE FROM ${tableName} WHERE id = ?`, [id],
            err => {
                if (err) throw err;
                observer.next(true);
            });
    });
};