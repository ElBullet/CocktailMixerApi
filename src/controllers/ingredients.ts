import { Request, Response, RequestHandler } from 'express';
import { Ingredient } from '../models/ingredient';
import * as ingredients from '../services/ingredient-service';
import { check, validationResult } from 'express-validator/check';
import { IngredientCategory } from '../models/ingredient-category';
import { IngredientUnit } from '../models/ingredient-unit';

const validators: RequestHandler[] = [
  check('name'),
  check('category').isIn(Object.keys(IngredientCategory)).withMessage('Value should be one of these: ' + Object.keys(IngredientCategory)),
  check('img').optional().isBase64(),
  check('unit').optional().isIn(Object.keys(IngredientUnit)).withMessage('Value should be one of these: ' + Object.keys(IngredientUnit)),
];

/**
 * GET /api/ingredients
 * Ingredients endpoint.
 */
export const create = validators.concat([
  (req: Request, res: Response) => {
    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const ingredient = req.body as Ingredient;
    ingredients.add(ingredient).subscribe(
      () => {
        res.status(201).json(ingredient);
      }, err => {
        throw err;
      }
    );
  }
]);

export const getAll = (req: Request, res: Response) => {
  ingredients.getAll(req.query).subscribe(
    (result) => {
      res.json(result);
    }, err => {
      throw err;
    }
  );
};

export const get = (req: Request, res: Response) => {
  if (!req.params.id) {
    return res.sendStatus(404);
  }

  ingredients.find(req.params.id).subscribe(
    (ingredient) => {
      if (!ingredient) {
        return res.sendStatus(404);
      }

      res.json(ingredient);
    }, err => {
      throw err;
    }
  );
};

export const update = validators.concat([
  (req: Request, res: Response) => {
    const ingredient = req.body as Ingredient;
    if (!req.params.id) {
      return res.sendStatus(404);
    } else if (req.params.id !== ingredient.id) {
      return res.status(400).send('The id in the url and body must match.');
    }

    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    ingredients.update(ingredient).subscribe(
      () => {
        res.sendStatus(204);
      }, err => {
        throw err;
      }
    );
  }
]);

export const remove = (req: Request, res: Response) => {
  ingredients.remove(req.params.id).subscribe(
    () => {
      res.sendStatus(204);
    }, err => {
      throw err;
    }
  );
};
