import { Request, Response, RequestHandler } from 'express';
import * as cocktails from '../services/cocktail-service';
import * as ingredients from '../services/ingredient-service';
import { Cocktail } from '../models/cocktail';
import { check, validationResult } from 'express-validator/check';
import { CocktailCategory } from '../models/cocktail-category';

const validators: RequestHandler[] = [
  check('name'),
  check('category').isIn(Object.keys(CocktailCategory)).withMessage('Value should be one of these: ' + Object.keys(CocktailCategory)),
  check('image').optional().isBase64(),
  check('ingredients').isArray().custom(value => {
    if (value.length < 2) {
      return Promise.reject();
    }
    else {
      return Promise.resolve();
    }
  }).withMessage('Please provide at least 2 ingredients'),
  check('ingredients.*.ingredient_id').custom(value => {
    return new Promise((resolve, reject) => {
      ingredients.exists(value).subscribe((exists) => {
        if (!exists) {
          reject();
        }
        else {
          resolve();
        }
      });
    });
  }).withMessage('Ingredient does not exist'),
  check('ingredients.*.amount').optional().isNumeric(),
];

/**
 * GET /api/cocktails
 * Cocktails endpoint.
 */
export const create = validators.concat([
  (req: Request, res: Response) => {
    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const cocktail = req.body as Cocktail;
    cocktails.add(cocktail).subscribe(
      () => {
        res.status(201).json(cocktail);
      }, err => {
        throw err;
      }
    );
  }
]);

export const getAll = (_req: Request, res: Response) => {
  cocktails.getAll().subscribe(
    (result) => {
      res.json(result);
    }, err => {
      throw err;
    }
  );
};

export const getIngredients = (req: Request, res: Response) => {
  if (!req.params.id) {
    return res.sendStatus(404);
  }

  cocktails.getIngredients(req.params.id).subscribe(
    (result) => {
      res.json(result);
    }, err => {
      throw err;
    }
  );
};

export const get = (req: Request, res: Response) => {
  if (!req.params.id) {
    return res.sendStatus(404);
  }

  cocktails.find(req.params.id).subscribe(
    (cocktail) => {
      if (!cocktail) {
        return res.sendStatus(404);
      }

      res.json(cocktail);
    }, err => {
      throw err;
    }
  );
};

export const update = validators.concat([
  (req: Request, res: Response) => {
    const cocktail = req.body as Cocktail;
    if (!req.params.id) {
      return res.sendStatus(404);
    } else if (req.params.id !== cocktail.id) {
      return res.status(400).send('The id in the url and body must match.');
    }

    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    cocktails.update(cocktail).subscribe(
      () => {
        res.sendStatus(204);
      }, err => {
        throw err;
      }
    );
  }
]);

export const remove = (req: Request, res: Response) => {
  cocktails.remove(req.params.id).subscribe(
    () => {
      res.sendStatus(204);
    }, err => {
      throw err;
    }
  );
};
