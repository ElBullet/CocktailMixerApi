import express from 'express';
import dotenv from 'dotenv';
import errorHandler from 'errorhandler';
import bodyParser = require('body-parser');
import cors = require('cors');

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env.example' });

// Create Express server
const app = express();

// Express configuration
app.set('port', 3000);

/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(cors({
  origin: /http:\/\/localhost:*/,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

export default app;