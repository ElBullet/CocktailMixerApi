CREATE TABLE IF NOT EXISTS `cocktail` (
    `id` INT NOT NULL  AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `category` VARCHAR(32) NOT NULL,
    `image_src` VARCHAR(128),
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `ingredient` (
    `id` INT NOT NULL  AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `category` VARCHAR(32) NOT NULL,
    `image_src` VARCHAR(128),
    `unit` VARCHAR(16),
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `cocktail_ingredient` (
    `cocktail_id` INT NOT NULL,
    `ingredient_id` INT NOT NULL,
    `amount` INT,
    CONSTRAINT `fk_cocktail`
		FOREIGN KEY (`cocktail_id`) REFERENCES `cocktail` (`id`)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT,
    CONSTRAINT `fk_ingredient`
		FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT
);
